#include <gtkmm.h>
#include <lv2gui.hpp>
#include "ksi.peg"
#include <lv2_uri_map.h>
#include <iostream>

using namespace sigc;
using namespace Gtk;
using namespace std;

#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

class KsiGUI : public LV2::GUI<KsiGUI, LV2::URIMap<true>, LV2::WriteMIDI<true> >{
public:

  KsiGUI(const char* URI)
    : m_button("Note")
  {

    Table* table = manage(new Table(9, 2));
    table->set_row_spacing(0, 10);
    table->set_homogeneous(true);

    g_scale = manage(new VScale(p_ports[p_gain].min,
				p_ports[p_gain].max, 0.01));
    p_scale = manage(new VScale(p_ports[p_pwm].min,
				p_ports[p_pwm].max, 0.01));
    s_scale = manage(new HScale(p_ports[p_ste].min,
				p_ports[p_ste].max, 0.01));

    g_scale->set_inverted(true);
    p_scale->set_inverted(true);

    slot<void> g_slot = compose(bind<0>(mem_fun(*this, &KsiGUI::write_control), p_gain),
				mem_fun(*g_scale, &VScale::get_value));
    slot<void> p_slot = compose(bind<0>(mem_fun(*this, &KsiGUI::write_control), p_pwm),
				mem_fun(*p_scale, &VScale::get_value));
    slot<void> s_slot = compose(bind<0>(mem_fun(*this, &KsiGUI::write_control), p_ste),
				mem_fun(*s_scale, &HScale::get_value));

    g_scale->signal_value_changed().connect(g_slot);
    p_scale->signal_value_changed().connect(p_slot);
    s_scale->signal_value_changed().connect(s_slot);

    table->attach(*g_scale, 0, 1, 0, 3);
    table->attach(*p_scale, 1, 2, 0, 3);
    table->attach(*manage(new Label("Gain")), 0, 1, 3, 4);
    table->attach(*manage(new Label("PWM")), 1, 2, 3, 4);
    table->attach(*manage(new Gtk::Image("/usr/local/lib/lv2/ksi.lv2/ksi.png")), 0, 1, 4, 5);
    table->attach(*manage(new Gtk::Image("/usr/local/lib/lv2/ksi.lv2/ksi.png")), 1, 2, 4, 5);
    table->attach(m_button, 0, 2, 5, 6);
    m_button.signal_clicked().connect(sigc::mem_fun(*this, &KsiGUI::send_event));
    table->attach(*manage(new Label("Stereo")), 0, 2, 7, 8);
    table->attach(*s_scale, 0, 2, 8, 9);
    add(*table);
  }

  void port_event(uint32_t port, uint32_t buffer_size, uint32_t format, const void* buffer) {
    if (port == p_gain)
      g_scale->set_value(*static_cast<const float*>(buffer));
    else if (port == p_pwm)
      p_scale->set_value(*static_cast<const float*>(buffer));
    else if (port == p_ste)
      s_scale->set_value(*static_cast<const float*>(buffer));
  }

protected:

  void send_event() {
    uint8_t noteon[] = { 0x90, 0x5A, 0x45, 0x5A, 0x45 };
    write_midi(0, ARRAY_SIZE(noteon), noteon);

    if (write_midi(0, ARRAY_SIZE(noteon), noteon))
      cout <<"Yup"<<endl;
    else
      cout <<"Nope"<<endl;

    cout <<ARRAY_SIZE(noteon)<<endl;
    // printf("Hi!\n");
  }
  Gtk::Button m_button;

  VScale* g_scale;
  VScale* p_scale;
  HScale* s_scale;

};

static int _ = KsiGUI::register_class("https://bitbucket.org/xaccrocheur/ksi#gui");
