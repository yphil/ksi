#include <lv2synth.hpp>
#include <cstdlib>
#include "ksi.peg"

class BeepVoice : public LV2::Voice {
public:

  BeepVoice(double rate)
    : m_key(LV2::INVALID_KEY), m_rate(rate), m_period(10), m_counter(0) {
  }

  void on(unsigned char key, unsigned char velocity) {
    m_key = key;
    m_period = m_rate * 4.0 / LV2::key2hz(m_key);
    m_envelope = velocity / 128.0;
    // m_pos = std::rand() / float(RAND_MAX);
    printf("Key: %d, Period: %d, Vel: %d, Envelope: %f, Pos: %f\n", key, m_period, velocity, m_envelope, m_pos);
  }

  void onoff(unsigned char key, unsigned char velocity) {
    m_key = key;
  }

  void off(unsigned char velocity) {
    m_key = LV2::INVALID_KEY;
  }

  unsigned char get_key() const {
    return m_key;
  }

  void render(uint32_t from, uint32_t to) {
    if (m_key == LV2::INVALID_KEY)
      return;
    for (uint32_t i = from; i < to; ++i) {

      float pwm = *p(p_pwm) + (1 - *p(p_pwm)) * m_envelope;
      float s = -0.25 + 0.5 * (m_counter > m_period * (1 + pwm) / 2);
      float position = *p(p_ste);

      m_counter = (m_counter + 1) % m_period;
      p(p_left)[i] += (1 - position) * s;
      p(p_right)[i] += position * s;
      if (m_envelope > 0)
	m_envelope -= 0.5 / m_rate;
    }
  }

protected:

  unsigned char m_key;
  double m_rate;
  uint32_t m_period;
  uint32_t m_counter;
  float m_envelope;
  float m_pos;

};


class Beep : public LV2::Synth<BeepVoice, Beep> {
public:

  Beep(double rate)
    : LV2::Synth<BeepVoice, Beep>(p_n_ports, p_midi) {
    add_voices(new BeepVoice(rate), new BeepVoice(rate), new BeepVoice(rate));

    add_audio_outputs(p_left, p_right);
  }

  void post_process(uint32_t from, uint32_t to) {
    for (uint32_t i = from; i < to; ++i) {
      p(p_left)[i] *= *p(p_gain);
      p(p_right)[i] *= *p(p_gain);
    }
  }

};

static int _ = Beep::register_class(p_uri);
